console.log('Hello, World')
// Details
const details = {
    fName: "Cielo",
    lName: "Corda",
    age: 18,
    hobbies : [
        "sewing", "watching", "reading"
    ] ,
    workAddress: {
        housenumber: "3354",
        street: "Mariposa Street",
        city: "Caloocan City",
        state: "Tired State",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");